package fi.vamk.e1800958.senddemo;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@GetMapping(value ="/test")
    public String test() {
        return "{\"id\":1}";
    }


}
